﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookShop.Domain.Abstract;
using BookShop.Domain.Entities;
using BookShop.WebUI.Models;

namespace BookShop.WebUI.Controllers
{
    public class BookController : Controller
    {
        private IBookRepository repository;
        public int pageSize = 4;

        public BookController(IBookRepository repo)
        {
            repository = repo;
        }

        // GET: Book
        /*public ActionResult Index()
        {
            return View();
        }*/

        public ViewResult List(string category, int page=1)
        {

            BooksListViewModel model = new BooksListViewModel
            {
                Books = repository.books
                   .Where(p => category == null || p.Category == category)
                   .OrderBy(game => game.BookId)
                   .Skip((page - 1) * pageSize)
                   .Take(pageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = category == null ?
                    repository.books.Count() :
                    repository.books.Where(game => game.Category == category).Count()
                },
                CurrentCategory = category
            };
            return View(model);
        }
    }
}