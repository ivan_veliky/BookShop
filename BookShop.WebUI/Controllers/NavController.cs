﻿using BookShop.Domain.Abstract;
using BookShop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookShop.WebUI.Controllers
{
    public class NavController : Controller
    {
        IBookRepository repository;

        public NavController(IBookRepository repo)
        {
            repository = repo;
        }

        public PartialViewResult Menu(string category = null)
        {
            ViewBag.SelectedCategory = category;

            IEnumerable<string> categories = repository.books
                .Where(book => book.Category != null)
                .Select(book => book.Category)
                .Distinct()
                .OrderBy(x => x);

            return PartialView(categories);
        }
    }
}