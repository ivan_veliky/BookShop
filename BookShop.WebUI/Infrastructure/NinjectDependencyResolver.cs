﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Mvc;
using Moq;
using Ninject;
using BookShop.Domain.Abstract;
using BookShop.Domain.Entities;
using BookShop.Domain.Concrete;


namespace BookShop.WebUI.Infrastructure
{
    class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            throw new NotImplementedException();
        }

        private void AddBindings()
        {
            /* Mock<IBookRepository> mockBooks = new Mock<IBookRepository>();
             mockBooks.Setup(m => m.Books).Returns(new List<Book>
             {
                 new Book { BookName = "C# Reference", Price = 1499 },
                 new Book { BookName = "How to read book properly", Price=2299 },
                 new Book { BookName = "How to become a millionaire", Price=899.4M }
             });

             kernel.Bind<IBookRepository>().ToConstant(mockBooks.Object);*/

            kernel.Bind<IBookRepository>().To<EFBookRepository>();

            EmailSettings emailSettings = new EmailSettings
            {
                WriteAsFile = bool.Parse(ConfigurationManager
                   .AppSettings["Email.WriteAsFile"] ?? "false")
            };

            kernel.Bind<IOrderProcessor>().To<EmailOrderProcessor>()
                .WithConstructorArgument("settings", emailSettings);
        }

    }
}
