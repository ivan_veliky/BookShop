﻿using BookShop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.WebUI.Models
{
    public class BooksListViewModel
    {
        public IEnumerable<Book> Books { get; set; } 
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}