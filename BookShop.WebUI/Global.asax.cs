﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using BookShop.Domain.Concrete;
using BookShop.WebUI.Models;
using BookShop.Domain.Entities;
using BookShop.WebUI.Infrastructure.Binders;

namespace BookShop.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());

            // Database.SetInitializer<EFDbContext>(null);
            //Database.SetInitializer(new BookDbInitializer());
        }
    }
}
