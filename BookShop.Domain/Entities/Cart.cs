﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Domain.Entities
{
    public class Cart
    {
        public List<CartLine> lineCollection = new List<CartLine>(); 

        public void AddItem(Book bookParam, int quantity)
        {
            CartLine line = lineCollection
                .Where(b => b.Book.BookId == bookParam.BookId)
                .FirstOrDefault();

            if(line == null)
            {
                lineCollection.Add(new CartLine()
                {
                    Book = bookParam,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Book book)
        {
            lineCollection.RemoveAll(l => l.Book.BookId == book.BookId);
        }
         
        public void Clear()
        {
            lineCollection.Clear();
        }

        public decimal ComputeTotalValue()
        {
            return (decimal)lineCollection.Sum(e => e.Book.Price * e.Quantity);
        }

        public IEnumerable<CartLine> Lines
        {
            get
            {
                return lineCollection;
            }
        }
    }

    public class CartLine
    {
        public Book Book { get; set; }
        public int Quantity { get; set; } 
    }
}
