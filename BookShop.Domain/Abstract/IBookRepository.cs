﻿using BookShop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Domain.Abstract
{
    public interface IBookRepository
    {
        IEnumerable<Book> books { get; }
    }
}
