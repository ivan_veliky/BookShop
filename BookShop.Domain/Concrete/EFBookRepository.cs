﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookShop.Domain.Entities;
using BookShop.Domain.Abstract;

namespace BookShop.Domain.Concrete
{
    public class EFBookRepository : IBookRepository
    {
        EFDbContext context = new EFDbContext();
        public IEnumerable<Book> books  
        {
            get
            {
                return context.books;
            }
        }
    }
}
