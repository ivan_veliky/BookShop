﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BookShop.Domain.Entities;
using System.Linq;
using BookShop.WebUI.Controllers;
using BookShop.WebUI.Models;
using BookShop.WebUI.HtmlHelpers;
using System.Web.Mvc;
using BookShop.Domain.Abstract;
using Moq;

namespace UnitTests
{

    [TestClass]
    public class CartTest
    {
        [TestMethod]
        public void Can_Add_New_Lines()
        {
            //arrange
            Book book1 = new Book() { BookId = 1, BookName = "Test1" };
            Book book2 = new Book() { BookId = 2, BookName = "Test2" };

            Cart cart = new Cart();

            //act
            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);

            List<CartLine> result = cart.Lines.ToList();

            //assert
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Book, book1);
            Assert.AreEqual(result[1].Book, book2);
        }

        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            //arrange
            Book book1 = new Book() { BookId = 1, BookName = "Test1" };
            Book book2 = new Book() { BookId = 2, BookName = "Test2" };

            Cart cart = new Cart();

            //act
            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);
            cart.AddItem(book1, 5);

            List<CartLine> result = cart.Lines.OrderBy(b => b.Book.BookId).ToList();

            //assert
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Quantity, 6);
            Assert.AreEqual(result[1].Quantity, 1);
        }

        [TestMethod]
        public void Can_Remove_Lines()
        {
            //arrange
            Book book1 = new Book() { BookId = 1, BookName = "Test1" };
            Book book2 = new Book() { BookId = 2, BookName = "Test2" };
            Book book3 = new Book() { BookId = 3, BookName = "Test3" };

            Cart cart = new Cart();

            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);
            cart.AddItem(book3, 5);
            cart.AddItem(book2, 4);

            //act
            cart.RemoveLine(book2);

            //assert
            Assert.AreEqual(cart.Lines.Where(c => c.Book == book2).Count(), 0);
            Assert.AreEqual(cart.Lines.Count(), 2);
        }

        [TestMethod]
        public void Can_Calculate_Cart_Total()
        {
            //arrange
            Book book1 = new Book() { BookId = 1, BookName = "Test1", Price = 100 };
            Book book2 = new Book() { BookId = 2, BookName = "Test2", Price = 200 };

            Cart cart = new Cart();

            //act
            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);
            cart.AddItem(book1, 2);

            decimal total = cart.ComputeTotalValue();

            //assert
            Assert.AreEqual(total, 500);
        }

        [TestMethod]
        public void Can_Clear_Content()
        {
            //arrange
            Book book1 = new Book() { BookId = 1, BookName = "Test1" };
            Book book2 = new Book() { BookId = 2, BookName = "Test2" };

            Cart cart = new Cart();

            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);
            cart.AddItem(book2, 4);

            //act
            cart.Clear();

            //assert
            Assert.AreEqual(cart.Lines.Count(), 0);
        }

        [TestMethod]
        public void Can_Add_To_Cart()
        {
           //arrange
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.books).Returns(new List<Book> {
            new Book {BookId = 1, BookName = "Игра1", Category = "Кат1"},
                      }.AsQueryable());

            
            Cart cart = new Cart();

            
            CartController controller = new CartController(mock.Object, null);

            // act
            controller.AddToCart(cart, 1, null);

            // assert
            Assert.AreEqual(cart.Lines.Count(), 1);
            Assert.AreEqual(cart.Lines.ToList()[0].Book.BookId, 1);
        }

        /// <summary>
        /// После добавления игры в корзину, должно быть перенаправление на страницу корзины
        /// </summary>
        [TestMethod]
        public void Adding_Game_To_Cart_Goes_To_Cart_Screen()
        {
            // arrange
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.books).Returns(new List<Book> {
        new Book {BookId = 1, BookName = "Игра1", Category = "Кат1"},
    }.AsQueryable());

           
            Cart cart = new Cart();

          
            CartController controller = new CartController(mock.Object, null);

            // act
            RedirectToRouteResult result = controller.AddToCart(cart, 2, "myUrl");

            // assert
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }

        // Проверяем URL
        [TestMethod]
        public void Can_View_Cart_Contents()
        {
            // arrange
            Cart cart = new Cart();

        
            CartController target = new CartController(null, null);

            // act
            CartIndexViewModel result
                = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;

            // assert
            Assert.AreSame(result.Cart, cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }
    }
}
