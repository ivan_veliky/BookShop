﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BookShop.Domain.Abstract;
using System.Collections.Generic;
using BookShop.Domain.Entities;
using BookShop.WebUI.Controllers;
using System.Linq;
using BookShop.WebUI.Models;
using BookShop.WebUI.HtmlHelpers;
using System.Web.Mvc;

namespace BookShop.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Paginate() 
        {
            //arrange
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Price = 0},
                new Book() { BookName = "Test2", Price = 0},
                new Book() { BookName = "Test3", Price = 0},
                new Book() { BookName = "Test4", Price = 0},
                new Book() { BookName = "Test5", Price = 0},
            });
            BookController controller = new BookController(fakeRepo.Object);
            controller.pageSize = 3;

            //act
            BooksListViewModel result = (BooksListViewModel)controller.List(null, 2).Model;

            //assert
            List<Book> books = result.Books.ToList();

            Assert.IsTrue(books.Count == 2);
            Assert.AreEqual(books[0].BookName, "Test4");
            Assert.AreEqual(books[1].BookName, "Test5");

        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            //arrange
            HtmlHelper myHelper = null;

            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            Func<int, string> pageUrlDelegate = i => "Page" + i;

            //act
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            //assert
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>"
               + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>"
               + @"<a class=""btn btn-default"" href=""Page3"">3</a>",
               result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model() 
        {
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Price = 0},
                new Book() { BookName = "Test2", Price = 0},
                new Book() { BookName = "Test3", Price = 0},
                new Book() { BookName = "Test4", Price = 0},
                new Book() { BookName = "Test5", Price = 0},
            });
            BookController controller = new BookController(fakeRepo.Object);
            controller.pageSize = 3;

            // Act
            BooksListViewModel result
                = (BooksListViewModel)controller.List(null, 2).Model;

            // Assert
            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);
        }

        [TestMethod]
        public void Can_Filter_Books() 
        {
            //arrange
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Category = "Cat1"},
                new Book() { BookName = "Test2", Category = "Cat2"},
                new Book() { BookName = "Test3", Category = "Cat1"},
                new Book() { BookName = "Test4", Category = "Cat2"},
                new Book() { BookName = "Test5", Category = "Cat1"},
            });
            BookController controller = new BookController(fakeRepo.Object);
            controller.pageSize = 3;

            //act
            List<Book> result = ((BooksListViewModel)
                controller.List("cat1", 1).Model).Books.ToList();

            //assert
            Assert.AreEqual(result.Count(), 3);
            Assert.IsTrue(result[0].BookName == "Test1" && result[0].Category == "Cat1");
            Assert.IsTrue(result[1].BookName == "Test3" && result[1].Category == "Cat1");
        }

        [TestMethod]
        public void Can_Create_Categories()
        {
            // arrange
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Category = "Educational"},
                new Book() { BookName = "Test2", Category = "Sci-Fi"},
                new Book() { BookName = "Test3", Category = "Educational"},
                new Book() { BookName = "Test4", Category = "Sci-Fi"},
                new Book() { BookName = "Test5", Category = "Art"},
            });

            NavController target = new NavController(fakeRepo.Object);

            // act
            List<string> results = ((IEnumerable<string>)target.Menu().Model).ToList();

            // assert
            Assert.AreEqual(results.Count(), 3);
            Assert.AreEqual(results[0], "Art");
            Assert.AreEqual(results[1], "Educational");
            Assert.AreEqual(results[2], "Sci-Fi");
        }

        [TestMethod]
        public void Indicates_Selected_Category()
        {
            // arrange
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Category = "Educational"},
                new Book() { BookName = "Test2", Category = "Sci-Fi"},
            });

            NavController target = new NavController(fakeRepo.Object);

            string categoryToSelect = "Art";

            // act
            string result = target.Menu(categoryToSelect).ViewBag.SelectedCategory;

            // assert
            Assert.AreEqual(categoryToSelect, result);
        }

        [TestMethod]
        public void Generate_Category_Specific_Game_Count()
        {
            // arrange
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Category = "Cat1"},
                new Book() { BookName = "Test2", Category = "Cat2"},
                new Book() { BookName = "Test3", Category = "Cat1"},
                new Book() { BookName = "Test4", Category = "Cat2"},
                new Book() { BookName = "Test5", Category = "Cat3"},
            });

            BookController controller = new BookController(fakeRepo.Object);
            controller.pageSize = 3;

            //act
            int res1 = ((BooksListViewModel)controller.List("Cat1").Model).PagingInfo.TotalItems;
            int res2 = ((BooksListViewModel)controller.List("Cat2").Model).PagingInfo.TotalItems;
            int res3 = ((BooksListViewModel)controller.List("Cat3").Model).PagingInfo.TotalItems;
            int resAll = ((BooksListViewModel)controller.List(null).Model).PagingInfo.TotalItems;

            // Утверждение
            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);
        }

    }
}
