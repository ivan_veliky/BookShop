﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BookShop.Domain.Abstract;
using System.Collections.Generic;
using BookShop.Domain.Entities;
using BookShop.WebUI.Controllers;

namespace BookShop.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CanPaginate()
        {
            //arrange
            Mock<IBookRepository> fakeRepo = new Mock<IBookRepository>();
            fakeRepo.Setup(m => m.books).Returns(new List<Book>()
            {
                new Book() { BookName = "Test1", Price = 0},
                new Book() { BookName = "Test2", Price = 0},
                new Book() { BookName = "Test3", Price = 0},
                new Book() { BookName = "Test4", Price = 0},
                new Book() { BookName = "Test5", Price = 0},
            });
            BookController controller = new BookController(fakeRepo.Object);
            controller.pageSize = 3;

            //act
            //IEnumerable<Book> result = (IEnumerable<Book>)controller.List(2).Model;

            //assert
        }
    }
}
